package com.cauli.netdisk;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author Cauli
 */
@ComponentScan(value = "com.cauli.netdisk.*")
@MapperScan("com.cauli.netdisk.**.mapper")
@EnableCaching
@SpringBootApplication
public class NetdiskApplication {
    public static void main(String[] args) {
        SpringApplication.run(NetdiskApplication.class, args);
    }
}
