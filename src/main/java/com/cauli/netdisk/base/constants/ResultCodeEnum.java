package com.cauli.netdisk.base.constants;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author Cauli
 * @date 2022/12/12 17:38
 * @description 全局返回结果码 枚举类
 */
@Getter
@AllArgsConstructor
public enum ResultCodeEnum {
    SUCCESS(200,"成功"),

    FAILURE(0,"失败"),

    INVALID_REQUEST(400,"无效请求"),

    NOT_LOGIN(401,"未登录"),

    NOT_JUR(403,"无权限"),

    REQUIRED_PASSWORD(405,"未输入密码"),

    INVALID_PASSWORD(406,"无效的密码"),

    WARNING(501,"警告");

    @EnumValue
    private final Integer code;

    @JsonValue
    private final String message;
}
