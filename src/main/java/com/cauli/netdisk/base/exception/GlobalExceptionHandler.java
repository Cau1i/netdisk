package com.cauli.netdisk.base.exception;

import cn.dev33.satoken.exception.NotLoginException;
import cn.dev33.satoken.exception.NotPermissionException;
import cn.dev33.satoken.exception.NotRoleException;
import com.cauli.netdisk.base.utils.ResultResponse;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * @author Cauli
 * @date 2022/12/12 17:38
 * @description 全局异常处理类
 */
@Slf4j
@ControllerAdvice
public class GlobalExceptionHandler {
    /**
     * 自定义异常处理方法
     *
     * @param e
     * @return
     */
    @ExceptionHandler(MyException.class)
    @ResponseBody
    public ResultResponse<?> fail(MyException e) {
        errorFixedPosition(e);
        log.error("_> 错误原因：");
        log.error("_> {}", e.getMessage());
        log.error("=============================错误打印完毕=============================");
        return ResultResponse.build(e.getCode(), e.getMessage());
    }

    /**
     * 处理一般异常
     *
     * @param e 参数
     * @return 返回异常信息
     */
    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public ResultResponse<String> exceptionHandler(Exception e) {
        errorFixedPosition(e);
        log.error("_> 错误原因：");
        log.error("_> {}", e.getMessage());
        log.error("=============================错误打印完毕=============================");
        return ResultResponse.fail("系统错误");
    }

    /**
     * 未登录
     *
     * @param e
     * @return
     */
    @ExceptionHandler(value = NotLoginException.class)
    @ResponseBody
    public ResultResponse<String> notLoginExceptionHandler(NotLoginException e) {
        errorFixedPosition(e);
        log.error("_> 错误原因：");
        log.error("_> {}", e.getMessage());
        log.error("=============================错误打印完毕=============================");
        return ResultResponse.fail("用户未登录");
    }

    /**
     * 角色异常
     *
     * @param e
     * @return
     */
    @ExceptionHandler(value = {NotRoleException.class, NotPermissionException.class})
    @ResponseBody
    public ResultResponse<String> notRoleExceptionHandler(Exception e) {
        errorFixedPosition(e);
        log.error("_> 错误原因：");
        log.error("_> {}", e.getMessage());
        log.error("=============================错误打印完毕=============================");
        return ResultResponse.fail("用户权限不足");
    }

    /**
     * 参数异常
     *
     * @param e
     * @return
     */
    @ExceptionHandler(value = ConstraintViolationException.class)
    @ResponseBody
    public ResultResponse<List<String>> constraintViolationHandler(ConstraintViolationException e) {
        Set<ConstraintViolation<?>> violations = e.getConstraintViolations();
        LinkedList<String> errors = Lists.newLinkedList();
        if (!CollectionUtils.isEmpty(violations)) {
            violations.forEach(constraintViolation -> errors.add(constraintViolation.getMessage()));
        }

        errorFixedPosition(e);
        log.error("_> 错误原因：");
        log.error("_> {}", e.getMessage());
        log.error("=============================错误打印完毕=============================");
        return ResultResponse.fail("无效参数", errors);
    }

    /**
     * 方法参数异常
     *
     * @param e
     * @return
     */
    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    @ResponseBody
    public ResultResponse<List<String>> methodArgumentNotValidExceptionHandler(MethodArgumentNotValidException e) {
        List<ObjectError> allErrors = e.getBindingResult().getAllErrors();
        LinkedList<String> errors = Lists.newLinkedList();
        if (!CollectionUtils.isEmpty(allErrors)) {
            allErrors.forEach(objectError -> errors.add(objectError.getDefaultMessage()));
        }

        errorFixedPosition(e);
        log.error("_> 错误原因：");
        log.error("_> {}", e.getMessage());
        log.error("=============================错误打印完毕=============================");
        return ResultResponse.fail("无效方法参数", errors);
    }

    /**
     * 处理空指针的异常
     *
     * @param e 参数
     * @return 返回异常信息
     */
    @ExceptionHandler(value = NullPointerException.class)
    @ResponseBody
    public ResultResponse<String> exceptionHandler(NullPointerException e) {
        errorFixedPosition(e);
        log.error("_> 错误原因：");
        log.error("_> {}", e.getMessage());
        log.error("=============================错误打印完毕=============================");
        return ResultResponse.fail("空指针异常");
    }

    /**
     * 定位错误发生的位置
     *
     * @param e 错误参数
     */
    private void errorFixedPosition(Exception e) {
        final StackTraceElement stackTrace = e.getStackTrace()[0];
        final String className = stackTrace.getClassName();
        final int lineNumber = stackTrace.getLineNumber();
        final String methodName = stackTrace.getMethodName();
        e.printStackTrace();
        log.error("=============================错误信息如下=============================");
        log.error("_> 异常定位：");
        log.error("_> 类[{}] ==> 方法[{}] ==> 所在行[{}]", className, methodName, lineNumber);
    }
}
