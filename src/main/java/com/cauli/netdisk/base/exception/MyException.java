package com.cauli.netdisk.base.exception;

import com.cauli.netdisk.base.constants.ResultCodeEnum;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * @author Cauli
 * @date 2022/12/12 17:38
 * @description 自定义全局异常类
 */
@Data
@ToString
@EqualsAndHashCode(callSuper=false)
public class MyException extends RuntimeException {
    private Integer code;

    /**
     * 通过状态码和错误消息创建异常对
     *
     * @param message
     */
    public MyException(String message) {
        super(message);
        this.code = 0;
    }

    /**
     * 通过状态码和错误消息创建异常对
     *
     * @param code
     * @param message
     */
    public MyException(Integer code, String message) {
        super(message);
        this.code = code;
    }

    /**
     * 接收枚举类型对象
     *
     * @param resultCodeEnum
     */
    public MyException(ResultCodeEnum resultCodeEnum) {
        super(resultCodeEnum.getMessage());
        this.code = resultCodeEnum.getCode();
    }
}
