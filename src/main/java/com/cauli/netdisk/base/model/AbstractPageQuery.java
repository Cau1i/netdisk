package com.cauli.netdisk.base.model;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.Max;

/**
 * @author Cauli
 * @date 2022/12/13 11:27
 * @description 页面查询参数 抽象类
 */
@Data
@EqualsAndHashCode(callSuper=false)
public abstract class AbstractPageQuery extends AbstractQuery{
    public static final int MAX_PAGE_NUM = 200;
    public static final int MAX_PAGE_SIZE = 500;

    @Max(MAX_PAGE_NUM)
    protected Integer pageNum = 1;
    @Max(MAX_PAGE_SIZE)
    protected Integer pageSize = 10;

    public Page toPage() {
        return new Page<>(pageNum, pageSize);
    }
}
