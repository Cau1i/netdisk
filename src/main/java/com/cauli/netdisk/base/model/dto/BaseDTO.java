package com.cauli.netdisk.base.model.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author Cauli
 * @date 2022/12/14 10:27
 * @description 传输类基类
 */
@Data
public class BaseDTO implements Serializable {
    @ApiModelProperty(value = "创建者ID", hidden = true)
    private Long creatorId;

    @ApiModelProperty(value = "创建时间", hidden = true)
    private LocalDateTime createTime;

    @ApiModelProperty(value = "更新者ID", hidden = true)
    private Long updaterId;

    @ApiModelProperty(value = "更新时间", hidden = true)
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "备注", hidden = true)
    private String remark;

    @ApiModelProperty(value = "删除标志（0代表存在 1代表删除）", hidden = true)
    private Integer deleted;
}
