package com.cauli.netdisk.base.model.dto;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.cauli.netdisk.base.model.AbstractPageQuery;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

/**
 * @author Cauli
 * @date 2022/12/14 10:27
 * @description 查询传输类基类
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class BaseQueryDTO extends AbstractPageQuery {
    @ApiModelProperty(value = "创建者ID" , hidden = true)
    private Long creatorId;

    @ApiModelProperty(value = "创建时间" , hidden = true)
    private LocalDateTime createTime;

    @ApiModelProperty(value = "更新者ID" , hidden = true)
    private Long updaterId;

    @ApiModelProperty(value = "更新时间" , hidden = true)
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "备注" , hidden = true)
    private String remark;

    @ApiModelProperty(value = "删除标志（0代表存在 1代表删除）" , hidden = true)
    private Integer deleted;


    @Override
    public QueryWrapper toQueryWrapper() {
        return null;
    }

    @Override
    public LambdaQueryWrapper toLambdaQueryWrapper() {
        return null;
    }
}
