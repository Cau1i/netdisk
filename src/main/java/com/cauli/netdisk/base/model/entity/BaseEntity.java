package com.cauli.netdisk.base.model.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author Cauli
 * @date 2022/12/13 11:27
 * @description 实体基类
 */
@Data
public class BaseEntity implements Serializable {
    /**
     * 创建者ID
     */
    @TableField(value = "creator_id")
    private Long creatorId;

    /**
     * 创建时间
     */
    @TableField(value = "create_time")
    private LocalDateTime createTime;

    /**
     * 更新者ID
     */
    @TableField(value = "updater_id")
    private Long updaterId;

    /**
     * 更新时间
     */
    @TableField(value = "update_time")
    private LocalDateTime updateTime;

    /**
     * 备注
     */
    @TableField(value = "remark")
    private String remark;

    /**
     * 删除标志（0代表存在 1代表删除）
     */
    @TableField(value = "deleted")
    private Integer deleted;
}