package com.cauli.netdisk.base.utils;

import com.cauli.netdisk.base.constants.ResultCodeEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

/**
 * @author Cauli
 * @date 2022/12/12 17:24
 * @description Ajax请求返回JSON格式数据的封装
 */
@Data
@ToString
public class ResultResponse<T> implements Serializable {
    @ApiModelProperty(value = "业务状态码：200为正常，其他值均为异常", example = "200")
    private final Integer code;

    @ApiModelProperty(value = "响应消息", example = "ok")
    private String msg;

    @ApiModelProperty(value = "响应数据")
    private T data;

    @ApiModelProperty(value = "数据总条数，分页情况有效")
    private final Long dataCount;

    @ApiModelProperty(value = "跟踪ID")
    private String traceId;

    /**
     * 构造函数
     */
    public ResultResponse(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
        this.data = null;
        this.dataCount = null;
    }

    public ResultResponse(Integer code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
        this.dataCount = null;
    }

    public ResultResponse(Integer code, String msg, T data, Long dataCount) {
        this.code = code;
        this.msg = msg;
        this.data = data;
        this.dataCount = dataCount;
    }

    /**
     * 给msg赋值，连缀风格
     */
    public ResultResponse<T> setMsg(String msg) {
        this.msg = msg;
        return this;
    }

    /**
     * 给data赋值，连缀风格
     */
    public ResultResponse<T> setData(T data) {
        this.data = data;
        return this;
    }

    /**
     * 返回成功
     */
    public static <T> ResultResponse<T> ok() {
        return new ResultResponse<>(ResultCodeEnum.SUCCESS.getCode(), "ok");
    }

    public static <T> ResultResponse<T> ok(String msg) {
        return new ResultResponse<>(ResultCodeEnum.SUCCESS.getCode(), msg);
    }

    public static <T> ResultResponse<T> ok(T data) {
        return new ResultResponse<>(ResultCodeEnum.SUCCESS.getCode(), "ok", data);
    }

    public static <T> ResultResponse<T> ok(String msg, T data) {
        return new ResultResponse<>(ResultCodeEnum.SUCCESS.getCode(), msg, data);
    }

    public static ResultResponse<Object> okArray(Object... data) {
        return new ResultResponse<>(ResultCodeEnum.SUCCESS.getCode(), "ok", data);
    }

    /**
     * 返回失败
     */
    public static <T> ResultResponse<T> fail() {
        return new ResultResponse<>(ResultCodeEnum.FAILURE.getCode(), "error");
    }

    public static <T> ResultResponse<T> fail(String msg) {
        return new ResultResponse<>(ResultCodeEnum.FAILURE.getCode(), msg);
    }

    public static <T> ResultResponse<T> fail(T data) {
        return new ResultResponse<>(ResultCodeEnum.FAILURE.getCode(), "error", data);
    }

    public static <T> ResultResponse<T> fail(String msg, T data) {
        return new ResultResponse<>(ResultCodeEnum.FAILURE.getCode(), msg, data);
    }

    public static ResultResponse<List<String>> fail(String msg, List<String> data) {
        return new ResultResponse<>(ResultCodeEnum.FAILURE.getCode(), msg, data);
    }

    public static <T> ResultResponse<T> badRequestError(String msg) {
        return new ResultResponse<>(ResultCodeEnum.INVALID_REQUEST.getCode(), msg);
    }

    /**
     * 返回警告
     */
    public static <T> ResultResponse<T> warning() {
        return new ResultResponse<>(ResultCodeEnum.WARNING.getCode(), "warning");
    }

    public static <T> ResultResponse<T> warning(String msg) {
        return new ResultResponse<>(ResultCodeEnum.WARNING.getCode(), msg);
    }

    public static <T> ResultResponse<T> notLogin() {
        return new ResultResponse<>(ResultCodeEnum.NOT_LOGIN.getCode(), "未登录，请登录后再次访问");
    }

    public static <T> ResultResponse<T> notJur(String msg) {
        return new ResultResponse<>(ResultCodeEnum.NOT_JUR.getCode(), msg);
    }

    /**
     * 返回一个自定义状态码
     */
    public static <T> ResultResponse<T> build(Integer code, String msg) {
        return new ResultResponse<>(code, msg);
    }

    /**
     * 返回分页和数据
     */
    public static ResultResponse<Object> getPageData(Long dataCount, Object data) {
        return new ResultResponse<>(ResultCodeEnum.SUCCESS.getCode(), "ok", data, dataCount);
    }

    /**
     * 返回，根据布尔值来确定最终结果(true = ok, false = fail)
     */
    public static <T> ResultResponse<T> getByBoolean(boolean b) {
        return b ? ok("ok") : fail("fail");
    }
}
