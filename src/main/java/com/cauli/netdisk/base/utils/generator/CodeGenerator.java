package com.cauli.netdisk.base.utils.generator;

import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.baomidou.mybatisplus.generator.config.rules.DateType;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Cauli
 * @date 2023/3/6 16:37
 * @description Mybatis-Plus代码生成器
 */
public class CodeGenerator {
    // url
    private static String url = "jdbc:mysql://localhost:3306/netdisk?serverTimezone=GMT%2B8&useSSL=false";

    // 用户名
    private static String username = "root";

    // 密码
    private static String password = "011023";

    // 表名
    private static String tableName = "config";

    // 父包名
    private static String parentName = "com.cauli.netdisk.module";

    // 指定输出目录
    private static String outputDir = "C:\\Users\\14248\\IdeaProjects\\netdisk\\src\\main\\resources";

    // xml生成路径
    private static String pathInfo = "C:\\Users\\14248\\IdeaProjects\\netdisk\\src\\main\\resources\\mapper";

    private static void generator() {
        FastAutoGenerator.create(url, username, password)
                // 全局配置
                .globalConfig(builder -> {
                    builder.enableSwagger()                                                 // 是否启用swagger注解
                            .author("Cauli")                                                // 作者名称
                            .dateType(DateType.ONLY_DATE)                                   // 时间策略
                            .commentDate("yyyy-MM-dd")                               // 注释日期
                            .outputDir(outputDir)                                           // 输出目录
                            .disableOpenDir();                                              // 生成后禁止打开所生成的系统目录
                })
                // 包配置
                .packageConfig(builder -> {
                    builder.parent(parentName)                                              // 父包名
                            .moduleName(tableName)                                          // 模块包名
                            .controller("controller")                                       // controller包名
                            .entity("model.entity")                                         // 实体类包名
                            .service("service")                                             // service包名
                            .serviceImpl("service.impl")                                    // serviceImpl包名
                            .mapper("mapper")                                               // mapper包名
                            .pathInfo(Collections.singletonMap(OutputFile.xml, pathInfo));  // 设置xml生成路径
                })
                // 策略配置
                .strategyConfig(builder -> {
                    builder.addInclude(tableName)                                           // 设置需要生成的表名
                            .addTablePrefix("t_", "c_");                                    // 设置过滤表前缀
                    builder.entityBuilder()                                                 // entity策略配置
                            .enableLombok()                                                 // 开启lombok
                            .enableTableFieldAnnotation();                                  // 开启生成实体时生成的字段注解
                    builder.controllerBuilder()                                             // controller 策略配置
                            .enableHyphenStyle()                                            // 开启生成@RestController控制器
                            .enableRestStyle();                                             // 驼峰转连字符
                    builder.mapperBuilder()                                                 // mapper策略配置
                            .enableMapperAnnotation()                                       // 开启@Mapper
                            .enableBaseColumnList()                                         // 启用 columnList (通用查询结果列)
                            .enableBaseResultMap()                                          // 启动resultMap
                            .formatMapperFileName("%sMapper")                               // Mapper 文件名称
                            .formatXmlFileName("%sMapper");                                 // Xml 文件名称
                    builder.serviceBuilder()                                                // service策略配置
                            .formatServiceFileName("%sService")                             // Service 文件名称
                            .formatServiceImplFileName("%sServiceImpl");                    // ServiceImpl 文件名称
                })
                // 注入配置
                .injectionConfig((scanner, builder) -> {
                    // 自定义vo，ro，qo等数据模型
                    Map<String, String> customFile = new HashMap<>();
                    customFile.put("DTO.java", "templates/model/dto.java.vm");
                    customFile.put("QueryDTO.java", "templates/model/query-dto.java.vm");
                    customFile.put("AddDTO.java", "templates/model/add-dto.java.vm");
                    customFile.put("UpdateDTO.java", "templates/model/update-dto.java.vm");
                    customFile.put("UpdateStatusDTO.java", "templates/model/update-status-dto.java.vm");
                    // 自定义配置对象
                    Map<String, Object> customMap = new HashMap<>();
                    customMap.put("dto", "DTO");
                    customMap.put("QueryDTO", "QueryDTO");
                    customMap.put("AddDTO", "AddDTO");
                    customMap.put("UpdateDTO", "UpdateDTO");
                    customMap.put("UpdateStatusDTO", "UpdateStatusDTO");
                    builder.customFile(customFile)                                          // 自定义模板
                            .customMap(customMap);                                          // 自定义map
                })
                .execute();
    }

    public static void main(String[] args) {
        generator();
    }
}

