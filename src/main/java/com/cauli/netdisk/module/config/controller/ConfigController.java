package com.cauli.netdisk.module.config.controller;

import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.SecureUtil;
import com.cauli.netdisk.base.utils.ResultResponse;
import com.cauli.netdisk.module.config.model.dto.ConfigDTO;
import com.cauli.netdisk.module.config.model.dto.UpdateLinkSettingRequestDTO;
import com.cauli.netdisk.module.config.model.dto.UpdateSecuritySettingRequestDTO;
import com.cauli.netdisk.module.config.model.dto.UpdateSiteSettingRequestDTO;
import com.cauli.netdisk.module.config.model.dto.UpdatePwdRequestDTO;
import com.cauli.netdisk.module.config.model.dto.UpdateViewSettingRequestDTO;
import com.cauli.netdisk.module.config.service.ConfigService;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSort;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * @author Cauli
 * @date 2022/12/15 15:08
 * @description 系统设置表 前端控制器
 */
@Api(tags = "系统设置表")
@ApiSort(2)
@RestController
@RequestMapping("/admin")
public class ConfigController {
    @Autowired
    private ConfigService configService;

    @ApiOperationSupport(order = 1)
    @ApiOperation(value = "获取站点信息")
    @GetMapping("/config")
    public ResultResponse<ConfigDTO> getConfig() {
        ConfigDTO config = configService.getConfig();
        return ResultResponse.ok(config);
    }

    @ApiOperationSupport(order = 2)
    @ApiOperation(value = "修改管理员账号密码")
    @PutMapping("/config/password")
    public ResultResponse<?> updatePwd(@Valid @RequestBody UpdatePwdRequestDTO settingRequestDTO) {
        ConfigDTO configDTO = new ConfigDTO();
        BeanUtils.copyProperties(settingRequestDTO, configDTO);
        if (StrUtil.isNotEmpty(settingRequestDTO.getPassword())) {
            configDTO.setPassword(SecureUtil.md5(configDTO.getPassword()));
        }
        configService.updateConfig(configDTO);
        return ResultResponse.ok();
    }

    @ApiOperationSupport(order = 3)
    @ApiOperation(value = "修改登录安全设置")
    @PutMapping("/config/security")
    public ResultResponse<?> updateSecuritySetting(@Valid @RequestBody UpdateSecuritySettingRequestDTO settingRequestDTO) {
        ConfigDTO configDTO = new ConfigDTO();
        BeanUtils.copyProperties(settingRequestDTO, configDTO);
        configService.updateConfig(configDTO);
        return ResultResponse.ok();
    }

    @ApiOperationSupport(order = 4)
    @ApiOperation(value = "修改站点设置")
    @PutMapping("/config/site")
    public ResultResponse<?> updateSiteSetting(@Valid @RequestBody UpdateSiteSettingRequestDTO settingRequestDTO) {
        ConfigDTO configDTO = new ConfigDTO();
        BeanUtils.copyProperties(settingRequestDTO, configDTO);
        configService.updateConfig(configDTO);
        return ResultResponse.ok();
    }


    @ApiOperationSupport(order = 5)
    @ApiOperation(value = "修改显示设置")
    @PutMapping("/config/view")
    public ResultResponse<?> updateViewSetting(@Valid @RequestBody UpdateViewSettingRequestDTO settingRequestDTO) {
        ConfigDTO configDTO = new ConfigDTO();
        BeanUtils.copyProperties(settingRequestDTO, configDTO);
        configService.updateConfig(configDTO);
        return ResultResponse.ok();
    }

    @ApiOperationSupport(order = 6)
    @ApiOperation(value = "修改直链设置")
    @PutMapping("/config/link")
    public ResultResponse<?> updateLinkSetting(@Valid @RequestBody UpdateLinkSettingRequestDTO settingRequestDTO) {
        ConfigDTO configDTO = new ConfigDTO();
        BeanUtils.copyProperties(settingRequestDTO, configDTO);
        configService.updateConfig(configDTO);
        return ResultResponse.ok();
    }

}