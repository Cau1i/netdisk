package com.cauli.netdisk.module.config.mapper;

import com.cauli.netdisk.module.config.model.entity.Config;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author Cauli
 * @date 2022-12-14 17:01
 * @description 系统设置属性表 参数实体映射
 */
@Mapper
public interface ConfigMapper extends BaseMapper<Config> {
    /**
     * 获取所有系统设置
     *
     * @return
     */
    List<Config> findAll();

    /**
     * 根据系统设置名称获取设置信息
     *
     * @param configName
     * @return
     */
    Config findByName(@Param("configName") String configName);


    /**
     * 批量保存系统设置
     *
     * @param configList
     * @return
     */
    int saveAll(@Param("configList") List<Config> configList);
}




