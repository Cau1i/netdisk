package com.cauli.netdisk.module.config.model.dto;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.cauli.netdisk.base.model.dto.BaseQueryDTO;
import com.cauli.netdisk.module.config.model.entity.Config;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author Cauli
 * @date 2023-03-07 14:13
 * @description config添加参数
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "ConfigQueryDTO", description = "config查询参数")
public class ConfigQueryDTO extends BaseQueryDTO {
  @ApiModelProperty("config名称")
  private String configName;

  @ApiModelProperty("config状态（0正常 1停用）")
  private Integer status;

  @Override
  public LambdaQueryWrapper<Config> toLambdaQueryWrapper() {
    LambdaQueryWrapper<Config> lambdaQueryWrapper = new LambdaQueryWrapper<>();
    lambdaQueryWrapper.like(StrUtil.isNotEmpty(configName), Config::getConfigName , configName);
    return lambdaQueryWrapper;
  }
}
