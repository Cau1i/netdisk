package com.cauli.netdisk.module.config.model.dto;

import com.cauli.netdisk.base.model.dto.BaseDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;

/**
 * @author Cauli
 * @date 2023-03-07 14:13
 * @description config更新参数
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "ConfigUpdateDTO", description = "config更新参数")
public class ConfigUpdateDTO extends BaseDTO {
    @NotNull(message = "configID不能为空")
    @ApiModelProperty("configID")
    private Long configId;

    @ApiModelProperty("系统设置属性 name")
    private String configName;

    @ApiModelProperty("系统设置属性 value")
    private String configValue;

    @ApiModelProperty("系统设置属性标题")
    private String configTitle;
}