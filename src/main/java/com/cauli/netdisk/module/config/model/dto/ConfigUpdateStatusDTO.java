package com.cauli.netdisk.module.config.model.dto;

import com.cauli.netdisk.base.model.dto.BaseDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;

/**
 * @author Cauli
 * @date 2023-03-07 14:13
 * @description config状态修改参数
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "ConfigUpdateStatusDTO", description = "config状态修改参数")
public class ConfigUpdateStatusDTO extends BaseDTO {
  @NotNull(message = "configID不能为空")
  @ApiModelProperty("configID")
  private Long configId;

  @NotNull(message = "config状态不能为空")
  @ApiModelProperty("config状态（0停用 1正常）")
  private Integer status;
}
