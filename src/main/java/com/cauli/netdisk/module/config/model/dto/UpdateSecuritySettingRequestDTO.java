package com.cauli.netdisk.module.config.model.dto;

import com.cauli.netdisk.module.login.model.enums.LoginVerifyModeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author Cauli
 * @date 2022/12/15 14:23
 * @description 登陆安全设置请求参数
 */
@Data
@ApiModel(description = "登陆安全设置请求参数")
public class UpdateSecuritySettingRequestDTO {
	@ApiModelProperty(value = "是否在前台显示登陆按钮", example = "true")
	private Boolean showLogin;

	@ApiModelProperty(value = "登陆验证方式，支持验证码和 2FA 认证")
	private LoginVerifyModeEnum loginVerifyMode;

	@ApiModelProperty(value = "登陆验证 Secret")
	private String loginVerifySecret;
}