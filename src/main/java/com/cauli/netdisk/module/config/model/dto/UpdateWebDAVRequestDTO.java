package com.cauli.netdisk.module.config.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author Cauli
 * @date 2022/12/15 14:23
 * @description WebDAV设置请求参数
 */
@Data
@ApiModel(description = "WebDAV设置请求参数")
public class UpdateWebDAVRequestDTO {
    @ApiModelProperty(value = "启用 WebDAV" , example = "true")
    private Boolean webdavEnable;

    @ApiModelProperty(value = "WebDAV 服务器中转下载" , example = "true")
    private Boolean webdavProxy;

    @ApiModelProperty(value = "WebDAV 账号" , example = "admin")
    private String webdavUsername;

    @ApiModelProperty(value = "WebDAV 密码" , example = "123456")
    private String webdavPassword;
}