package com.cauli.netdisk.module.config.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;

/**
 * @author Cauli
 * @date 2022-12-14 17:01
 * @description 系统设置属性表
 */
@Data
@TableName("config")
@ApiModel(value = "Config", description = "系统设置属性表")
public class Config implements Serializable {
    /**
     * 系统设置属性ID
     */
    @TableId(value = "config_id", type = IdType.AUTO)
    private Integer configId;

    /**
     * 系统设置属性名称
     */
    @TableField(value = "config_name")
    private String configName;

    /**
     * 系统设置属性值
     */
    @TableField(value = "config_value")
    private String configValue;

    /**
     * 系统设置属性标题
     */
    @TableField(value = "config_title")
    private String configTitle;
}