package com.cauli.netdisk.module.config.model.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author Cauli
 * @date 2022/12/15 14:23
 * @description 文件点击模式 枚举类：用于控制文件是单击打开还是双击打开
 */
@Getter
@AllArgsConstructor
public enum FileClickModeEnum {
	CLICK("click","单击打开文件/文件夹"),
	DBCLICK("dbclick","双击打开文件/文件夹");

	@EnumValue
	private final String value;

	@JsonValue
	private final String description;
}