package com.cauli.netdisk.module.config.model.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author Cauli
 * @date 2022/12/15 14:23
 * @description Referer防盗链类型 枚举类
 */
@Getter
@AllArgsConstructor
public enum RefererTypeEnum {
	OFF("off","不启用 Referer 防盗链"),
	WHITE_LIST("white_list","启用白名单模式"),
	BLACK_LIST("black_list","启用黑名单模式");

	@EnumValue
	private final String value;

	@JsonValue
	private final String description;
}