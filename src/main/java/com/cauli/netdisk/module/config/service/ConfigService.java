package com.cauli.netdisk.module.config.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.cauli.netdisk.module.config.model.dto.ConfigAddDTO;
import com.cauli.netdisk.module.config.model.dto.ConfigDTO;
import com.cauli.netdisk.module.config.model.dto.ConfigQueryDTO;
import com.cauli.netdisk.module.config.model.dto.ConfigUpdateDTO;
import com.cauli.netdisk.module.config.model.dto.ConfigUpdateStatusDTO;
import com.cauli.netdisk.module.config.model.entity.Config;

import java.util.List;

/**
 * @author Cauli
 * @date 2022-12-14 17:01:10
 * @description 系统设置属性表 服务实现类
 */
public interface ConfigService extends IService<Config> {
    /**
     * 分页获得config信息
     *
     * @param configQueryDTO
     * @return
     */
    Page<Config> getConfigPage(ConfigQueryDTO configQueryDTO);

    /**
     * 根据configID获取详细信息
     *
     * @param configId
     * @return
     */
    ConfigDTO getConfigById(Long configId);

    /**
     * 添加config
     *
     * @param configAddDTO
     */
    void addConfig(ConfigAddDTO configAddDTO);

    /**
     * 修改config
     *
     * @param configUpdateDTO
     */
    void updateConfig(ConfigUpdateDTO configUpdateDTO);

    /**
     * 修改config状态
     *
     * @param configUpdateStatusDTO
     */
    void changeConfigStatus(ConfigUpdateStatusDTO configUpdateStatusDTO);

    /**
     * 删除config
     *
     * @param configIds
     */
    void deleteConfig(List<Long> configIds);

    /**
     * 获取系统设置，如果缓存中有，则去缓存取，没有则查询数据库并写入到缓存中
     *
     * @return
     */
    ConfigDTO getConfig();

    /**
     * 更新系统设置, 并清空缓存中的内容
     *
     * @param configDTO
     */
    void updateConfig(ConfigDTO configDTO);
}
