package com.cauli.netdisk.module.login.controller;

import cn.dev33.satoken.stp.SaTokenInfo;
import cn.dev33.satoken.stp.StpUtil;
import com.cauli.netdisk.base.utils.ResultResponse;
import com.cauli.netdisk.module.config.model.dto.ConfigDTO;
import com.cauli.netdisk.module.config.service.ConfigService;
import com.cauli.netdisk.module.login.model.dto.LoginRequestDTO;
import com.cauli.netdisk.module.login.model.dto.LoginVerifyImgDTO;
import com.cauli.netdisk.module.login.service.ImgVerifyCodeService;
import com.cauli.netdisk.module.login.service.LoginService;
import com.cauli.netdisk.module.menu.model.dto.RouterDTO;
import com.cauli.netdisk.module.menu.service.MenuService;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSort;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

/**
 * @author Cauli
 * @date 2022/12/12 16:27
 * @description 登录 前端控制器
 */
@Api(tags = "登录")
@ApiSort(1)
@RestController
@RequestMapping("/login")
public class LoginController {
    @Autowired
    private LoginService loginService;

    @Autowired
    private ConfigService configService;

    @Autowired
    private ImgVerifyCodeService imgVerifyCodeService;

    @Autowired
    private MenuService menuService;

    @ApiOperationSupport(order = 1)
    @ApiOperation(value = "获取图形验证码")
    @GetMapping("/captcha")
    public ResultResponse<LoginVerifyImgDTO> captcha() {
        return ResultResponse.ok(imgVerifyCodeService.generatorCaptcha());
    }

    @ApiOperationSupport(order = 2)
    @ApiOperation(value = "查询验证码是否开启")
    @GetMapping("/captchaMode")
    public ResultResponse<Boolean> loginVerifyMode() {
        ConfigDTO configDTO = configService.getConfig();
        return ResultResponse.ok(configDTO.getCaptchaMode());
    }

    @ApiOperationSupport(order = 3)
    @ApiOperation(value = "登录")
    @PostMapping("/login")
    public ResultResponse<?> doLogin(@Valid @RequestBody LoginRequestDTO loginRequestDTO) {
        SaTokenInfo login = loginService.login(loginRequestDTO);
        if (login != null) {
            return ResultResponse.ok("登录成功", login);
        } else {
            return ResultResponse.fail("登录失败");
        }
    }

    @ApiOperationSupport(order = 4)
    @ApiOperation(value = "查询登录状态")
    @GetMapping("isLogin")
    public ResultResponse<Boolean> isLogin() {
        return ResultResponse.ok(StpUtil.isLogin());
    }

    @ApiOperationSupport(order = 5)
    @ApiOperation(value = "查询Token信息")
    @GetMapping("tokenInfo")
    public ResultResponse<SaTokenInfo> tokenInfo() {
        return ResultResponse.ok(StpUtil.getTokenInfo());
    }

    @ApiOperationSupport(order = 6)
    @ApiOperation(value = "注销")
    @PostMapping("logout")
    public ResultResponse<?> logout() {
        StpUtil.logout();
        return ResultResponse.ok("注销成功");
    }

    @ApiOperationSupport(order = 7)
    @ApiOperation(value = "获取路由信息")
    @GetMapping("/getRouters")
    public ResultResponse<List<RouterDTO>> getRouters() {
        long userId = StpUtil.getLoginIdAsLong();
        List<RouterDTO> routerList = menuService.getRouters(userId);
        return ResultResponse.ok(routerList);
    }
}
