package com.cauli.netdisk.module.login.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author Cauli
 * @date 2022-12-14 10:40:20
 * @description 生成图片验证码返回参数
 */
@Data
@ApiModel(description = "生成图片验证码返回参数")
public class LoginVerifyImgDTO {
	@ApiModelProperty(value = "验证码图片", example = "ok:image/png;base64,iajsiAAA...")
	private String imgBase64;

	@ApiModelProperty(value = "验证码 UUID", example = "c140a792-4ca2-4dac-8d4c-35750b78524f")
	private String uuid;
}