package com.cauli.netdisk.module.login.model.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author Cauli
 * @date 2022-12-14 10:40:50
 * @description 登陆验证方式 枚举类
 */
@Getter
@AllArgsConstructor
public enum LoginVerifyModeEnum {
	/**
	 * 不启用登陆模式
	 */
	OFF_MODE("off"),

	/**
	 * 图形验证码模式
	 */
	IMG_VERIFY_MODE("image");

	@EnumValue
	@JsonValue
	private final String value;
}