package com.cauli.netdisk.module.login.service;

import com.cauli.netdisk.module.login.model.dto.LoginVerifyImgDTO;

/**
 * @author Cauli
 * @date 2022/12/14 16:21
 * @description 图片验证码 服务类
 */
public interface ImgVerifyCodeService {
    /**
     * 生成验证码，并写入缓存中.
     *
     * @return 验证码生成结果
     */
    LoginVerifyImgDTO generatorCaptcha();

    /**
     * 对验证码进行验证, 如验证失败则抛出异常
     *
     * @param verifyCodeUUID 验证码UUID
     * @param verifyCode     验证码
     */
    void checkCaptcha(String verifyCodeUUID, String verifyCode);
}
