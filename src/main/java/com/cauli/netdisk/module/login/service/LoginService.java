package com.cauli.netdisk.module.login.service;

import cn.dev33.satoken.stp.SaTokenInfo;
import com.cauli.netdisk.module.login.model.dto.LoginRequestDTO;

/**
 * @author Cauli
 * @date 2022/12/13 10:04
 * @description 登录 服务类
 */
public interface LoginService {
    /**
     * 登录
     *
     * @param loginRequestDTO
     * @return
     */
    SaTokenInfo login(LoginRequestDTO loginRequestDTO);
}
