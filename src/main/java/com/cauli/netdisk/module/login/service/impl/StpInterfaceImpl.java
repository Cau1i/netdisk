package com.cauli.netdisk.module.login.service.impl;

import cn.dev33.satoken.stp.StpInterface;
import cn.hutool.core.convert.Convert;
import com.cauli.netdisk.module.menu.service.MenuService;
import com.cauli.netdisk.module.role.model.dto.RoleDTO;
import com.cauli.netdisk.module.role.service.RoleService;
import com.cauli.netdisk.module.menu.model.dto.MenuDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Cauli
 * @date 2022/12/13 11:28
 * @description 自定义权限验证接口扩展
 */
@Component
public class StpInterfaceImpl implements StpInterface {
    @Autowired
    private MenuService menuService;

    @Autowired
    private RoleService roleService;

    /**
     * 返回一个账号所拥有的权限码集合
     *
     * @param loginId
     * @param loginType
     * @return
     */
    @Override
    public List<String> getPermissionList(Object loginId, String loginType) {
        List<MenuDTO> menuDTOList = menuService.getPermissionList(Convert.toLong(loginId), null);
        return menuDTOList.stream().map(MenuDTO::getPermission).collect(Collectors.toList());
    }

    /**
     * 返回一个账号所拥有的角色标识集合
     *
     * @param loginId
     * @param loginType
     * @return
     */
    @Override
    public List<String> getRoleList(Object loginId, String loginType) {
        RoleDTO roleDTO = roleService.getRoleById(Convert.toLong(loginId));
        List<String> roleDTOList = new ArrayList<>();
        roleDTOList.add(roleDTO.getRoleKey());
        return roleDTOList;
    }

}
