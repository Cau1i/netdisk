package com.cauli.netdisk.module.menu.controller;

import cn.dev33.satoken.stp.StpUtil;
import cn.hutool.core.lang.tree.Tree;
import com.cauli.netdisk.base.utils.ResultResponse;
import com.cauli.netdisk.module.menu.model.dto.MenuAddDTO;
import com.cauli.netdisk.module.menu.model.dto.MenuDTO;
import com.cauli.netdisk.module.menu.model.dto.MenuQueryDTO;
import com.cauli.netdisk.module.menu.model.dto.MenuUpdateDTO;
import com.cauli.netdisk.module.menu.model.dto.MenuUpdateStatusDTO;
import com.cauli.netdisk.module.menu.model.dto.TreeSelectedDTO;
import com.cauli.netdisk.module.menu.model.entity.Menu;
import com.cauli.netdisk.module.menu.service.MenuService;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSort;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

/**
 * @author Cauli
 * @date 2022/12/28 14:18
 * @description 菜单表 前端控制器
 */
@Api(tags = "菜单表")
@ApiSort(2)
@RestController
@RequestMapping("/menu")
public class MenuController {
    @Autowired
    private MenuService menuService;

    @ApiOperationSupport(order = 1)
    @ApiOperation(value = "获取menu列表")
    @GetMapping("/list")
    public ResultResponse<List<Menu>> listMenu(MenuQueryDTO menuQueryDTO) {
        List<Menu> menuList = menuService.getMenuList(menuQueryDTO);
        return ResultResponse.ok(menuList);
    }

    @ApiOperationSupport(order = 2)
    @ApiOperation(value = "根据menuID获取详细信息")
    @GetMapping("/{menuId}")
    public ResultResponse<MenuDTO> getMenu(@PathVariable(value = "menuId", required = false) Long menuId) {
        MenuDTO menuDTO = menuService.getMenuById(menuId);
        return ResultResponse.ok(menuDTO);
    }

    @ApiOperationSupport(order = 3)
    @ApiOperation(value = "新增menu")
    @PostMapping
    public ResultResponse<?> add(@Valid @RequestBody MenuAddDTO menuAddDTO) {
        menuService.addMenu(menuAddDTO);
        return ResultResponse.ok("添加成功");
    }

    @ApiOperationSupport(order = 4)
    @ApiOperation(value = "修改menu")
    @PutMapping
    public ResultResponse<?> edit(@Validated @RequestBody MenuUpdateDTO menuUpdateDTO) {
        menuService.updateMenu(menuUpdateDTO);
        return ResultResponse.ok("修改成功");
    }

    @ApiOperationSupport(order = 5)
    @ApiOperation(value = "menu状修改态")
    @PutMapping("/status")
    public ResultResponse<?> changeStatus(@Valid @RequestBody MenuUpdateStatusDTO menuUpdateStatusDTO) {
        menuService.changeMenuStatus(menuUpdateStatusDTO);
        return ResultResponse.ok("修改成功");
    }

    @ApiOperationSupport(order = 6)
    @ApiOperation(value = "删除menu")
    @DeleteMapping(value = "/{menuIds}")
    public ResultResponse<?> remove(@PathVariable List<Long> menuIds) {
        menuService.deleteMenu(menuIds);
        return ResultResponse.ok("删除成功");
    }

    @ApiOperationSupport(order = 7)
    @ApiOperation(value = "获取用户菜单下拉树列表")
    @GetMapping("/dropdownList")
    public ResultResponse<List<Tree<Long>>> dropdownList() {
        long userId = StpUtil.getLoginIdAsLong();
        List<Tree<Long>> dropdownList = menuService.getDropdownList(userId);
        return ResultResponse.ok(dropdownList);
    }

    @ApiOperationSupport(order = 8)
    @ApiOperation(value = "加载对应角色菜单列表树")
    @GetMapping(value = "/roleMenuTreeSelect/{roleId}")
    public ResultResponse<TreeSelectedDTO> roleMenuTreeSelect(@PathVariable("roleId") Long roleId) {
        long userId = StpUtil.getLoginIdAsLong();
        TreeSelectedDTO roleDropdownList = menuService.getRoleDropdownList(userId, roleId);
        return ResultResponse.ok(roleDropdownList);
    }
}
