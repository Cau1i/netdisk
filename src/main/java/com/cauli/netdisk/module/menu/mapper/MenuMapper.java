package com.cauli.netdisk.module.menu.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cauli.netdisk.module.menu.model.dto.MenuDTO;
import com.cauli.netdisk.module.menu.model.entity.Menu;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author Cauli
 * @date 2022-12-13 15:15:47
 * @description 菜单权限表 参数实体映射
 */
@Mapper
public interface MenuMapper extends BaseMapper<Menu> {
    /**
     * 根据用户ID和菜单类型查询权限信息
     *
     * @param userId
     * @param menuType
     * @return
     */
    List<MenuDTO> getPermissionList(@Param("userId") Long userId, @Param("menuType") Integer menuType);

    /**
     * 根据用户ID查询菜单列表
     *
     * @param userId
     * @return
     */
    List<Menu> getMenuListByUserId(Long userId);

    /**
     * 根据角色ID查询菜单列表
     *
     * @param roleId
     * @return
     */
    List<Long> getMenuIdListByRoleId(Long roleId);
}




