package com.cauli.netdisk.module.menu.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cauli.netdisk.module.menu.model.entity.RoleMenu;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author Cauli
 * @date 2023/3/6 13:21
 * @description 角色和菜单关联表 参数实体映射
 */
@Mapper
public interface RoleMenuMapper extends BaseMapper<RoleMenu> {
}
