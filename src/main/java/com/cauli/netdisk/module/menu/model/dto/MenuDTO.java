package com.cauli.netdisk.module.menu.model.dto;

import com.cauli.netdisk.base.model.dto.BaseDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author Cauli
 * @date 2022/12/14 10:15
 * @description 菜单返回参数
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "MenuDTO", description = "菜单返回参数")
public class MenuDTO extends BaseDTO {
    @NotNull(message = "菜单ID不能为空")
    @ApiModelProperty("菜单ID")
    private Long menuId;

    @NotBlank(message = "菜单名称不能为空")
    @ApiModelProperty("菜单名称")
    private String menuName;

    @NotNull(message = "菜单名称不能为空")
    @ApiModelProperty("父菜单ID")
    private Long parentId;

    @NotNull(message = "显示顺序不能为空")
    @ApiModelProperty("显示顺序")
    private Integer orderNum;

    @ApiModelProperty("路由地址")
    private String path;

    @ApiModelProperty("组件路径")
    private String component;

    @ApiModelProperty("路由参数")
    private String query;

    @NotNull(message = "是否为外链（0否 1是）不能为空")
    @ApiModelProperty("是否为外链（0否 1是）")
    private Boolean isExternal;

    @NotNull(message = "是否缓存（0不缓存 1缓存）不能为空")
    @ApiModelProperty("是否缓存（0不缓存 1缓存）")
    private Boolean isCache;

    @NotNull(message = "菜单类型（M=1目录 C=2菜单 F=3按钮）不能为空")
    @ApiModelProperty("菜单类型（M=1目录 C=2菜单 F=3按钮）")
    private Integer menuType;

    @NotNull(message = "显示状态（0隐藏 1显示）")
    @ApiModelProperty("显示状态（0隐藏 1显示）")
    private Boolean isVisible;

    @ApiModelProperty("菜单状态（0停用 1正常）")
    private Integer status;

    @ApiModelProperty("权限标识")
    private String permission;

    @ApiModelProperty("菜单图标")
    private String icon;
}
