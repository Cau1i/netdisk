package com.cauli.netdisk.module.menu.model.dto;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.cauli.netdisk.base.model.dto.BaseQueryDTO;
import com.cauli.netdisk.module.menu.model.entity.Menu;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author Cauli
 * @date 2023-03-07 11:27
 * @description 菜单查询参数
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "MenuQueryDTO", description = "菜单查询参数")
public class MenuQueryDTO extends BaseQueryDTO {
  @ApiModelProperty("menu名称")
  private String menuName;

  @ApiModelProperty("菜单状态（0正常 1停用）")
  private Integer status;

  @Override
  public LambdaQueryWrapper<Menu> toLambdaQueryWrapper() {
    LambdaQueryWrapper<Menu> lambdaQueryWrapper = new LambdaQueryWrapper<>();
    lambdaQueryWrapper.like(StrUtil.isNotEmpty(menuName), Menu::getMenuName , menuName)
            .eq(status != null, Menu::getStatus , status)
            .eq(Menu::getDeleted , 0);
    return lambdaQueryWrapper;
  }
}
