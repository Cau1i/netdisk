package com.cauli.netdisk.module.menu.model.dto;

import com.cauli.netdisk.base.model.dto.BaseDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;

/**
 * @author Cauli
 * @date 2023-03-07 11:27
 * @description 菜单状态更新参数
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "MenuUpdateStatusDTO", description = "菜单状态更新参数")
public class MenuUpdateStatusDTO extends BaseDTO {
  @NotNull(message = "菜单ID不能为空")
  @ApiModelProperty("菜单ID")
  private Long menuId;

  @NotNull(message = "菜单状态（0停用 1正常）不能为空")
  @ApiModelProperty("菜单状态（0停用 1正常）")
  private Integer status;
}
