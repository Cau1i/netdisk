package com.cauli.netdisk.module.menu.model.dto;

import cn.hutool.core.lang.tree.Tree;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author Cauli
 * @date 2022/12/30 10:15
 * @description 对应角色菜单列表树返回参数
 */
@Data
@NoArgsConstructor
public class TreeSelectedDTO {
    @ApiModelProperty("menuID列表")
    private List<Long> checkedKeys;

    @ApiModelProperty("menu列表树")
    private List<Tree<Long>> menus;
}
