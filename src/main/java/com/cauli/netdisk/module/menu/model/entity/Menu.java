package com.cauli.netdisk.module.menu.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.cauli.netdisk.base.model.entity.BaseEntity;
import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 * @author Cauli
 * @date 2022-12-14 10:38:12
 * @description 菜单权限表
 */
@Data
@TableName("menu")
@ApiModel(value = "menu", description = "菜单权限表")
public class Menu extends BaseEntity {
    /**
     * 菜单ID
     */
    @TableId(value = "menu_id", type = IdType.AUTO)
    private Long menuId;

    /**
     * 菜单名称
     */
    @TableField(value = "menu_name")
    private String menuName;

    /**
     * 父菜单ID
     */
    @TableField(value = "parent_id")
    private Long parentId;

    /**
     * 显示顺序
     */
    @TableField(value = "order_num")
    private Integer orderNum;

    /**
     * 路由地址
     */
    @TableField(value = "path")
    private String path;

    /**
     * 组件路径
     */
    @TableField(value = "component")
    private String component;

    /**
     * 路由参数
     */
    @TableField(value = "query")
    private String query;

    /**
     * 是否为外链（0否 1是）
     */
    @TableField(value = "is_external")
    private Boolean isExternal;

    /**
     * 是否缓存（0不缓存 1缓存）
     */
    @TableField(value = "is_cache")
    private Boolean isCache;

    /**
     * 菜单类型（M=1目录 C=2菜单 F=3按钮）
     */
    @TableField(value = "menu_type")
    private Integer menuType;

    /**
     * 显示状态（0隐藏 1显示）
     */
    @TableField(value = "is_visible")
    private Boolean isVisible;

    /**
     * 菜单状态（0停用 1正常）
     */
    @TableField(value = "status")
    private Integer status;

    /**
     * 权限标识
     */
    @TableField(value = "permission")
    private String permission;

    /**
     * 菜单图标
     */
    @TableField(value = "icon")
    private String icon;
}
