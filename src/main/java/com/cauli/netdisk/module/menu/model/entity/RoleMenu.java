package com.cauli.netdisk.module.menu.model.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;

/**
 * @author Cauli
 * @date 2022-12-14 11:06:02
 * @description 角色和菜单关联表
 */
@Data
@TableName("role_menu")
@ApiModel(value = "RoleMenu", description = "角色和菜单关联表")
public class RoleMenu implements Serializable {
    /**
     * 角色ID
     */
    @TableId(value = "role_id")
    private Long roleId;

    /**
     * 菜单ID
     */
    @TableField(value = "menu_id")
    private Long menuId;
}