package com.cauli.netdisk.module.menu.model.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author Cauli
 * @date 2022-12-13 15:15:47
 * @description 菜单组件类型 枚举类
 */
@Getter
@AllArgsConstructor
public enum MenuComponentEnum  {
    LAYOUT(1,"Layout"),
    PARENT_VIEW(2,"ParentView"),
    INNER_LINK(3,"InnerLink");

    @EnumValue
    private final int value;

    @JsonValue
    private final String description;
}
