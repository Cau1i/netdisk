package com.cauli.netdisk.module.menu.model.enums;


import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author Cauli
 * @date 2022-12-13 15:15:47
 * @description 菜单类型 枚举类
 */
@Getter
@AllArgsConstructor
public enum MenuTypeEnum  {
    DIRECTORY(1, "目录"),
    MENU(2, "菜单"),
    BUTTON(3, "按钮");

    @EnumValue
    private final Integer value;

    @JsonValue
    private final String description;
}
