package com.cauli.netdisk.module.menu.service;

import cn.hutool.core.lang.tree.Tree;
import com.baomidou.mybatisplus.extension.service.IService;
import com.cauli.netdisk.module.menu.model.dto.MenuAddDTO;
import com.cauli.netdisk.module.menu.model.dto.MenuDTO;
import com.cauli.netdisk.module.menu.model.dto.MenuQueryDTO;
import com.cauli.netdisk.module.menu.model.dto.MenuUpdateDTO;
import com.cauli.netdisk.module.menu.model.dto.MenuUpdateStatusDTO;
import com.cauli.netdisk.module.menu.model.dto.RouterDTO;
import com.cauli.netdisk.module.menu.model.dto.TreeSelectedDTO;
import com.cauli.netdisk.module.menu.model.entity.Menu;

import java.util.List;

/**
 * @author Cauli
 * @date 2022-12-13 15:15:47
 * @description 菜单权限表 服务类
 */
public interface MenuService extends IService<Menu> {
    /**
     * 获得menu信息
     *
     * @param menuQueryDTO
     * @return
     */
    List<Menu> getMenuList(MenuQueryDTO menuQueryDTO);

    /**
     * 根据menuID获取详细信息
     *
     * @param menuId
     * @return
     */
    MenuDTO getMenuById(Long menuId);

    /**
     * 添加menu
     *
     * @param menuAddDTO
     */
    void addMenu(MenuAddDTO menuAddDTO);

    /**
     * 修改menu
     *
     * @param menuUpdateDTO
     */
    void updateMenu(MenuUpdateDTO menuUpdateDTO);

    /**
     * 修改menu状态
     *
     * @param menuUpdateStatusDTO
     */
    void changeMenuStatus(MenuUpdateStatusDTO menuUpdateStatusDTO);

    /**
     * 删除menu
     *
     * @param menuIds
     */
    void deleteMenu(List<Long> menuIds);

    /**
     * 返回一个账号所拥有的权限码集合
     *
     * @param userId
     * @param menuType
     * @return
     */
    List<MenuDTO> getPermissionList(Long userId, Integer menuType);

    /**
     * 获取用户菜单下拉树列表
     *
     * @param userId
     * @return
     */
    List<Tree<Long>> getDropdownList(Long userId);

    /**
     * 获取路由信息
     *
     * @param userId
     * @return
     */
    List<RouterDTO> getRouters(Long userId);

    /**
     * 加载对应角色菜单列表树
     *
     * @param userId
     * @param roleId
     * @return
     */
    TreeSelectedDTO getRoleDropdownList(Long userId, Long roleId);
}
