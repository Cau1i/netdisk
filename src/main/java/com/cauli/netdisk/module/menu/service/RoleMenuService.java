package com.cauli.netdisk.module.menu.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cauli.netdisk.module.menu.model.entity.RoleMenu;

/**
 * @author Cauli
 * @date 2023/3/6 13:17
 * @description 角色和菜单关联表 服务类
 */
public interface RoleMenuService extends IService<RoleMenu> {

}
