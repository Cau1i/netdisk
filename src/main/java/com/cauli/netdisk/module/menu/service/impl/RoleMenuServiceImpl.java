package com.cauli.netdisk.module.menu.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cauli.netdisk.module.menu.mapper.RoleMenuMapper;
import com.cauli.netdisk.module.menu.model.entity.RoleMenu;
import com.cauli.netdisk.module.menu.service.RoleMenuService;
import org.springframework.stereotype.Service;

/**
 * @author Cauli
 * @date 2023/3/6 13:18
 * @description 角色和菜单关联表 服务类
 */
@Service
public class RoleMenuServiceImpl extends ServiceImpl<RoleMenuMapper, RoleMenu> implements RoleMenuService {

}
