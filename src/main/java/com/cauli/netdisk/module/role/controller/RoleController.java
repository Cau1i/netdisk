package com.cauli.netdisk.module.role.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cauli.netdisk.base.model.page.PageDTO;
import com.cauli.netdisk.base.utils.ResultResponse;
import com.cauli.netdisk.module.role.model.dto.*;
import com.cauli.netdisk.module.role.model.entity.Role;
import com.cauli.netdisk.module.role.service.RoleService;
import com.cauli.netdisk.module.user.model.dto.UserDTO;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSort;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @author Cauli
 * @date 2023/2/19 15:08
 * @description 角色表 前端控制器
 */
@Api(tags = "角色表")
@ApiSort(3)
@RestController
@RequestMapping("/role")
public class RoleController {
    @Autowired
    private RoleService roleService;

    @ApiOperationSupport(order = 1)
    @ApiOperation(value = "获取role列表")
    @GetMapping("/list")
    public ResultResponse<Page<Role>> listRole(RoleQueryDTO roleQueryDTO) {
        Page<Role> rolePage = roleService.getRolePage(roleQueryDTO);
        return ResultResponse.ok(rolePage);
    }

    @ApiOperationSupport(order = 2)
    @ApiOperation(value = "根据roleID获取详细信息")
    @GetMapping("/{roleId}")
    public ResultResponse<RoleDTO> getRole(@PathVariable(value = "roleId", required = false) Long roleId) {
        RoleDTO roleDTO = roleService.getRoleById(roleId);
        return ResultResponse.ok(roleDTO);
    }

    @ApiOperationSupport(order = 3)
    @ApiOperation(value = "新增role")
    @PostMapping
    public ResultResponse<?> addRole(@Valid @RequestBody RoleAddDTO roleAddDTO) {
        roleService.addRole(roleAddDTO);
        return ResultResponse.ok("添加成功");
    }

    @ApiOperationSupport(order = 4)
    @ApiOperation(value = "修改role")
    @PutMapping
    public ResultResponse<?> editRole(@Validated @RequestBody RoleUpdateDTO roleUpdateDTO) {
        roleService.updateRole(roleUpdateDTO);
        return ResultResponse.ok("修改成功");
    }

    @ApiOperationSupport(order = 5)
    @ApiOperation(value = "role状态修改")
    @PutMapping("/status")
    public ResultResponse<?> changeRoleStatus(@Valid @RequestBody RoleUpdateStatusDTO roleUpdateStatusDTO) {
        roleService.changeRoleStatus(roleUpdateStatusDTO);
        return ResultResponse.ok("修改成功");
    }

    @ApiOperationSupport(order = 6)
    @ApiOperation(value = "删除role")
    @DeleteMapping(value = "/{roleIds}")
    public ResultResponse<?> removeRole(@PathVariable List<Long> roleIds) {
        roleService.deleteRole(roleIds);
        return ResultResponse.ok("删除成功");
    }

    @ApiOperationSupport(order = 7)
    @ApiOperation(value = "查询已分配角色的用户列表")
    @GetMapping("/{roleId}/allocated/list")
    public ResultResponse<PageDTO<UserDTO>> allocatedUserList(@PathVariable("roleId") Long roleId, AllocatedRoleQuery query) {
        query.setRoleId(roleId);
        PageDTO<UserDTO> page = roleService.getAllocatedUserList(query);
        return ResultResponse.ok(page);
    }

    @ApiOperationSupport(order = 8)
    @ApiOperation(value = "查询未分配角色的用户列表")
    @GetMapping("/{roleId}/unallocated/list")
    public ResultResponse<PageDTO<UserDTO>> unallocatedUserList(@PathVariable("roleId") Long roleId, UnallocatedRoleQuery query) {
        query.setRoleId(roleId);
        PageDTO<UserDTO> page = roleService.getUnallocatedUserList(query);
        return ResultResponse.ok(page);
    }

    @ApiOperationSupport(order = 9)
    @ApiOperation(value = "批量授权用户")
    @PostMapping("/{roleId}/users/{userIds}/grant/bulk")
    public ResultResponse<?> addRoleForUserByBulk(@PathVariable("roleId") Long roleId, @PathVariable("userIds") List<Long> userIds) {
        roleService.addRoleOfUserByBulk(roleId, userIds);
        return ResultResponse.ok("授权成功");
    }

    @ApiOperationSupport(order = 10)
    @ApiOperation(value = "批量取消授权用户")
    @DeleteMapping("/users/{userIds}/grant/bulk")
    public ResultResponse<?> deleteRoleOfUserByBulk(@PathVariable("userIds") List<Long> userIds) {
        roleService.deleteRoleOfUserByBulk(userIds);
        return ResultResponse.ok("取消授权成功");
    }
}
