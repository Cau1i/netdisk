package com.cauli.netdisk.module.role.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cauli.netdisk.module.role.model.dto.RoleDTO;
import com.cauli.netdisk.module.role.model.entity.Role;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cauli.netdisk.module.user.model.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author Cauli
 * @date 2022-12-13 15:16:02
 * @description 角色信息 参数实体映射
 */
@Mapper
public interface RoleMapper extends BaseMapper<Role> {
    /**
     * 根据 用户ID 查询 角色信息
     *
     * @param userId
     * @return
     */
    List<RoleDTO> getRoleList(@Param("userId") Long userId);

    /**
     * 根据 条件 查询 该角色授权或未授权的用户
     * @param toPage
     * @param queryWrapper
     * @return
     */
    Page<User> getUserListByRole(Page toPage, @Param(Constants.WRAPPER) Wrapper<User> queryWrapper);
}




