package com.cauli.netdisk.module.role.model.dto;

import com.cauli.netdisk.base.model.dto.BaseDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * @author Cauli
 * @date 2022/12/14 13:48
 * @description 角色返回参数
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "RoleDTO", description = "角色返回参数")
public class RoleDTO extends BaseDTO {
    @ApiModelProperty("用户ID")
    private Long userId;

    @ApiModelProperty("角色ID")
    private Long roleId;

    @ApiModelProperty("角色名称")
    private String roleName;

    @ApiModelProperty("角色权限字符串")
    private String roleKey;

    @ApiModelProperty("显示顺序")
    private Integer roleSort;

    @ApiModelProperty("角色状态（1正常 0停用）")
    private Integer status;

    @ApiModelProperty("路由ID列表")
    private List<Long> menuIds;
}
