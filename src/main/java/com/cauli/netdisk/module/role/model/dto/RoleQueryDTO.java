package com.cauli.netdisk.module.role.model.dto;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.cauli.netdisk.base.model.dto.BaseQueryDTO;
import com.cauli.netdisk.module.role.model.entity.Role;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author Cauli
 * @date 2023/2/19 15:18
 * @description 角色查询参数
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "RoleQueryDTO", description = "角色查询参数")
public class RoleQueryDTO extends BaseQueryDTO {
    @ApiModelProperty("角色名称")
    private String roleName;

    @ApiModelProperty("角色权限字符串")
    private String roleKey;

    @ApiModelProperty("角色状态（1正常 0停用）")
    private Integer status;

    @Override
    public LambdaQueryWrapper<Role> toLambdaQueryWrapper() {
        LambdaQueryWrapper<Role> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.like(StrUtil.isNotEmpty(roleName), Role::getRoleName, roleName)
                .like(StrUtil.isNotEmpty(roleKey), Role::getRoleKey, roleKey)
                .eq(status != null, Role::getStatus, status)
                .eq(Role::getDeleted, 0)
                .orderBy(true, true, Role::getRoleSort);
        return lambdaQueryWrapper;
    }
}
