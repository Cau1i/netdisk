package com.cauli.netdisk.module.role.model.dto;

import com.cauli.netdisk.base.model.dto.BaseDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author Cauli
 * @date 2023/3/5 17:20
 * @description 角色修改参数
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "RoleUpdateDTO", description = "角色修改参数")
public class RoleUpdateDTO extends BaseDTO {
    @ApiModelProperty("角色ID")
    private Long roleId;

    @NotBlank(message = "角色名称不能为空")
    @ApiModelProperty("角色名称")
    private String roleName;

    @NotBlank(message = "角色权限不能为空")
    @ApiModelProperty("角色权限字符串")
    private String roleKey;

    @NotNull(message = "显示顺序不能为空")
    @ApiModelProperty("显示顺序")
    private Integer roleSort;

    @NotNull(message = "角色状态不能为空")
    @ApiModelProperty("角色状态（1正常 0停用）")
    private Integer status;

    @NotNull
    @ApiModelProperty("菜单ID列表")
    private List<Long> menuIds;
}
