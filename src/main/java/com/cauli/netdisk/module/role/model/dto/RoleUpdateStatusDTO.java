package com.cauli.netdisk.module.role.model.dto;

import com.cauli.netdisk.base.model.dto.BaseDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;

/**
 * @author Cauli
 * @date 2022/12/16 11:05
 * @description 角色修改状态参数
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "RoleUpdateStatusDTO", description = "角色修改状态参数")
public class RoleUpdateStatusDTO extends BaseDTO {
    @NotNull(message = "角色ID不能为空")
    @ApiModelProperty("角色ID")
    private Long roleId;

    @NotNull(message = "角色状态不能为空")
    @ApiModelProperty("角色状态（0停用 1正常）")
    private Integer status;
}
