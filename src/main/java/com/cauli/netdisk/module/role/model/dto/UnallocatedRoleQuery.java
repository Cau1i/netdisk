package com.cauli.netdisk.module.role.model.dto;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.cauli.netdisk.base.model.dto.BaseQueryDTO;
import com.cauli.netdisk.module.user.model.entity.User;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;

/**
 * @author Cauli
 * @date 2023/3/6  12:03
 * @description 未授权角色查询参数
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "UnallocatedRoleQuery", description = "未授权角色查询参数")
public class UnallocatedRoleQuery extends BaseQueryDTO {
    @NotNull(message = "角色ID不能为空")
    @ApiModelProperty("角色ID")
    private Long roleId;

    @ApiModelProperty("角色ID")
    private String username;

    @ApiModelProperty("角色ID")
    private String phoneNumber;

    @Override
    public QueryWrapper<User> toQueryWrapper() {
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.like(StrUtil.isNotEmpty(username), "u.username", username)
                .like(StrUtil.isNotEmpty(phoneNumber), "u.phone_number", phoneNumber)
                .eq("u.role_id", 0);
        return queryWrapper;
    }
}
