package com.cauli.netdisk.module.role.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.cauli.netdisk.base.model.entity.BaseEntity;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author Cauli
 * @date 2022-12-14 10:41:15
 * @description 角色表
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("role")
@ApiModel(value = "Role", description = "角色表")
public class Role extends BaseEntity {
    /**
     * 角色ID
     */
    @TableId(value = "role_id", type = IdType.AUTO)
    private Long roleId;

    /**
     * 角色名称
     */
    @TableField(value = "role_name")
    private String roleName;

    /**
     * 角色权限字符串
     */
    @TableField(value = "role_key")
    private String roleKey;

    /**
     * 显示顺序
     */
    @TableField(value = "role_sort")
    private Integer roleSort;

    /**
     * 角色状态（1正常 0停用）
     */
    @TableField(value = "status")
    private Integer status;
}
