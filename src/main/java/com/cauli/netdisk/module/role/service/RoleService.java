package com.cauli.netdisk.module.role.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cauli.netdisk.base.model.page.PageDTO;
import com.cauli.netdisk.module.role.model.dto.*;
import com.cauli.netdisk.module.role.model.entity.Role;
import com.baomidou.mybatisplus.extension.service.IService;
import com.cauli.netdisk.module.user.model.dto.UserDTO;

import java.util.List;

/**
 * @author Cauli
 * @date 2022-12-13 15:16:02
 * @description 角色表 服务类
 */
public interface RoleService extends IService<Role> {
    /**
     * 分页获得Role信息
     *
     * @param roleQueryDTO
     * @return
     */
    Page<Role> getRolePage(RoleQueryDTO roleQueryDTO);

    /**
     * 根据RoleID获取详细信息
     *
     * @param roleId
     * @return
     */
    RoleDTO getRoleById(Long roleId);

    /**
     * 添加Role
     *
     * @param roleAddDTO
     */
    void addRole(RoleAddDTO roleAddDTO);

    /**
     * 修改Role
     *
     * @param roleUpdateDTO
     */
    void updateRole(RoleUpdateDTO roleUpdateDTO);

    /**
     * 修改Role状态
     *
     * @param roleUpdateStatusDTO
     */
    void changeRoleStatus(RoleUpdateStatusDTO roleUpdateStatusDTO);

    /**
     * 删除Role
     *
     * @param roleIds
     */
    void deleteRole(List<Long> roleIds);

    /**
     * 查询已分配Role的用户列表
     *
     * @param query
     * @return
     */
    PageDTO<UserDTO> getAllocatedUserList(AllocatedRoleQuery query);

    /**
     * 查询未分配用户Role列表
     *
     * @param query
     * @return
     */
    PageDTO<UserDTO> getUnallocatedUserList(UnallocatedRoleQuery query);

    /**
     * 批量授权用户
     * @param roleId
     * @param userIds
     */
    void addRoleOfUserByBulk(Long roleId, List<Long> userIds);

    /**
     * 批量取消授权用户
     * @param userIds
     */
    void deleteRoleOfUserByBulk(List<Long> userIds);
}
