package com.cauli.netdisk.module.user.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cauli.netdisk.base.utils.ResultResponse;
import com.cauli.netdisk.module.user.model.dto.UserAddDTO;
import com.cauli.netdisk.module.user.model.dto.UserDetailDTO;
import com.cauli.netdisk.module.user.model.dto.UserQueryDTO;
import com.cauli.netdisk.module.user.model.dto.UserUpdateDTO;
import com.cauli.netdisk.module.user.model.dto.UserUpdatePwdDTO;
import com.cauli.netdisk.module.user.model.dto.UserUpdateStatusDTO;
import com.cauli.netdisk.module.user.model.entity.User;
import com.cauli.netdisk.module.user.service.UserService;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSort;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

/**
 * @author Cauli
 * @date 2022/12/15 15:08
 * @description 用户表 前端控制器
 */
@Api(tags = "用户表")
@ApiSort(4)
@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;

    @ApiOperationSupport(order = 1)
    @ApiOperation(value = "获取user列表")
    @GetMapping("/list")
    public ResultResponse<Page<User>> listUser(UserQueryDTO userQueryDTO) {
        Page<User> userPage = userService.getUserPage(userQueryDTO);
        return ResultResponse.ok(userPage);
    }

    @ApiOperationSupport(order = 2)
    @ApiOperation(value = "根据userID获取详细信息")
    @GetMapping(value = {"/", "/{userId}"})
    public ResultResponse<UserDetailDTO> getUserDetail(@PathVariable(value = "userId", required = false) Long userId) {
        UserDetailDTO userDetailDTO = userService.getUserDetail(userId);
        return ResultResponse.ok(userDetailDTO);
    }

    @ApiOperationSupport(order = 3)
    @ApiOperation(value = "新增user")
    @PostMapping
    public ResultResponse<?> addUser(@Valid @RequestBody UserAddDTO userAddDTO) {
        userService.addUser(userAddDTO);
        return ResultResponse.ok("添加成功");
    }

    @ApiOperationSupport(order = 4)
    @ApiOperation(value = "修改user")
    @PutMapping
    public ResultResponse<?> updateUser(@RequestBody UserUpdateDTO userUpdateDTO) {
        userService.updateUser(userUpdateDTO);
        return ResultResponse.ok("修改成功");
    }

    @ApiOperationSupport(order = 5)
    @ApiOperation(value = "user状态修改")
    @PutMapping("/status")
    public ResultResponse<?> changeUserStatus(@Valid @RequestBody UserUpdateStatusDTO userUpdateStatusDTO) {
        userService.changeUserStatus(userUpdateStatusDTO);
        return ResultResponse.ok("修改成功");
    }

    @ApiOperationSupport(order = 6)
    @ApiOperation(value = "修改密码")
    @PutMapping("/password")
    public ResultResponse<?> resetUserPassword(@Valid @RequestBody UserUpdatePwdDTO userUpdatePwdDTO) {
        userService.resetUserPassword(userUpdatePwdDTO);
        return ResultResponse.ok("密码修改成功");
    }

    @ApiOperationSupport(order = 7)
    @ApiOperation(value = "删除user")
    @DeleteMapping("/{userIds}")
    public ResultResponse<?> deleteUser(@PathVariable List<Long> userIds) {
        userService.deleteUsers(userIds);
        return ResultResponse.ok("删除成功");
    }
}