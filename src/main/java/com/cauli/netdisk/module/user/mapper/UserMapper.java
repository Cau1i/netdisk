package com.cauli.netdisk.module.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cauli.netdisk.module.user.model.entity.User;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author Cauli
 * @date 2022-12-13 15:12:32
 * @description 用户表 参数实体映射
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {

}




