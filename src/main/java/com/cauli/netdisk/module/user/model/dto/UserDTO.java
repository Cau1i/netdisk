package com.cauli.netdisk.module.user.model.dto;

import com.cauli.netdisk.base.model.dto.BaseDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

/**
 * @author Cauli
 * @date 2022/12/15 17:33
 * @description 用户信息返回参数
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "UserDTO" , description = "用户信息返回参数")
public class UserDTO extends BaseDTO {
    @ApiModelProperty("用户ID")
    private Long userId;

    @ApiModelProperty("角色id")
    private Long roleId;

    @ApiModelProperty("用户账号")
    private String username;

    @ApiModelProperty("用户昵称")
    private String nickName;

    @ApiModelProperty("用户类型（0系统用户）")
    private Integer userType;

    @ApiModelProperty("用户邮箱")
    private String email;

    @ApiModelProperty("手机号码")
    private String phoneNumber;

    @ApiModelProperty("用户性别（0未知 1男 2女）")
    private Integer sex;

    @ApiModelProperty("头像地址")
    private String avatar;

    @ApiModelProperty("用户状态（0停用 1正常）")
    private Integer status;

    @ApiModelProperty("最后登录IP")
    private String loginIp;

    @ApiModelProperty("最后登录时间")
    private LocalDateTime loginDate;
}
