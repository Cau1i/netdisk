package com.cauli.netdisk.module.user.model.dto;

import com.cauli.netdisk.module.role.model.dto.RoleDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author Cauli
 * @date 2022/12/15 17:20
 * @description 用户详细信息返回参数
 */
@Data
@ApiModel(value = "UserDetailDTO" , description = "用户详细信息返回参数")
public class UserDetailDTO {
    @ApiModelProperty("用户信息")
    private UserDTO userDTO;

    @ApiModelProperty("用户角色")
    private RoleDTO roleDTO;

    @ApiModelProperty("用户权限集合")
    private List<String> permissions;
}
