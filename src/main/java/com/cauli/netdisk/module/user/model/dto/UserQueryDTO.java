package com.cauli.netdisk.module.user.model.dto;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.cauli.netdisk.base.model.dto.BaseQueryDTO;
import com.cauli.netdisk.module.user.model.entity.User;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author Cauli
 * @date 2022/12/15 15:18
 * @description 用户查询参数
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "UserQueryDTO" , description = "用户查询参数")
public class UserQueryDTO extends BaseQueryDTO {
    @ApiModelProperty("用户账号")
    private String username;

    @ApiModelProperty("手机号码")
    private String phoneNumber;

    @ApiModelProperty("用户状态（0停用 1正常）")
    private Integer status;

    @Override
    public LambdaQueryWrapper<User> toLambdaQueryWrapper() {
        LambdaQueryWrapper<User> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.like(StrUtil.isNotEmpty(username), User::getUsername , username)
                .like(StrUtil.isNotEmpty(phoneNumber), User::getPhoneNumber , phoneNumber)
                .eq(status != null, User::getStatus , status)
                .eq(User::getDeleted , 0);
        return lambdaQueryWrapper;
    }
}
