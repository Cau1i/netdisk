package com.cauli.netdisk.module.user.model.dto;

import com.cauli.netdisk.base.model.dto.BaseDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author Cauli
 * @date 2022/12/15 17:20
 * @description 用户修改参数
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "UserUpdateDTO", description = "用户修改参数")
public class UserUpdateDTO extends BaseDTO {
    @NotNull(message = "用户ID不能为空")
    @ApiModelProperty("用户ID")
    private Long userId;

    @NotNull(message = "角色id不能为空")
    @ApiModelProperty("角色id")
    private Long roleId;

    @NotBlank(message = "用户昵称不能为空")
    @ApiModelProperty("用户昵称")
    private String nickName;

    @ApiModelProperty("用户邮箱")
    private String email;

    @ApiModelProperty("手机号码")
    private String phoneNumber;

    @ApiModelProperty("用户性别（0未知 1男 2女）")
    private Integer sex;

    @ApiModelProperty("头像地址")
    private String avatar;

    @NotNull(message = "用户状态不能为空")
    @ApiModelProperty("用户状态（0停用 1正常）")
    private Integer status;

    @ApiModelProperty(value = "备注")
    private String remark;
}
