package com.cauli.netdisk.module.user.model.dto;

import com.cauli.netdisk.base.model.dto.BaseDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;

/**
 * @author Cauli
 * @date 2022/12/16 11:05
 * @description 用户修改密码参数
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "UserUpdatePwdDTO", description = "用户修改密码参数")
public class UserUpdatePwdDTO extends BaseDTO {
    @ApiModelProperty("用户ID")
    private Long userId;

    @ApiModelProperty("用户密码")
    private String oldPassword;

    @NotBlank(message = "用户新密码不能为空")
    @ApiModelProperty("用户新密码")
    private String newPassword;
}
