package com.cauli.netdisk.module.user.model.dto;

import com.cauli.netdisk.base.model.dto.BaseDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;

/**
 * @author Cauli
 * @date 2022/12/16 11:05
 * @description 用户修改状态参数
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "UserUpdateStatusDTO", description = "用户修改状态参数")
public class UserUpdateStatusDTO extends BaseDTO {
    @NotNull(message = "用户ID不能为空")
    @ApiModelProperty("用户ID")
    private Long userId;

    @NotNull(message = "用户状态不能为空")
    @ApiModelProperty("用户状态（0停用 1正常）")
    private Integer status;
}
