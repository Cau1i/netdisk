package com.cauli.netdisk.module.user.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.cauli.netdisk.module.user.model.dto.UserAddDTO;
import com.cauli.netdisk.module.user.model.dto.UserDetailDTO;
import com.cauli.netdisk.module.user.model.dto.UserQueryDTO;
import com.cauli.netdisk.module.user.model.dto.UserUpdateDTO;
import com.cauli.netdisk.module.user.model.dto.UserUpdatePwdDTO;
import com.cauli.netdisk.module.user.model.dto.UserUpdateStatusDTO;
import com.cauli.netdisk.module.user.model.entity.User;

import java.util.List;

/**
 * @author Cauli
 * @date 2022-12-13 15:12:32
 * @description 用户表 服务类
 */
public interface UserService extends IService<User> {
    /**
     * 分页获得用户信息
     *
     * @param userQueryDTO
     * @return
     */
    Page<User> getUserPage(UserQueryDTO userQueryDTO);

    /**
     * 根据用户ID获取详细信息
     *
     * @param userId
     * @return
     */
    UserDetailDTO getUserDetail(Long userId);

    /**
     * 添加用户
     *
     * @param userAddDTO
     */
    void addUser(UserAddDTO userAddDTO);

    /**
     * 修改用户
     *
     * @param userUpdateDTO
     */
    void updateUser(UserUpdateDTO userUpdateDTO);

    /**
     * 修改用户状态
     *
     * @param userUpdateStatusDTO
     */
    void changeUserStatus(UserUpdateStatusDTO userUpdateStatusDTO);

    /**
     * 修改用户密码
     *
     * @param userUpdatePwdDTO
     */
    void resetUserPassword(UserUpdatePwdDTO userUpdatePwdDTO);

    /**
     * 删除用户
     *
     * @param userIds
     */
    void deleteUsers(List<Long> userIds);

    /**
     * 检测 用户名/手机号码/邮箱 是否重复
     *
     * @param info   用户名/手机号码/邮箱
     * @param userId 当前用户ID
     * @param mark   0:用户名 1:手机号码 2:邮箱
     * @return
     */
    void checkIsDuplicated(String info, Long userId, int mark);

    /**
     * 获取个人信息
     * @return
     */
    UserDetailDTO getProfile();
}
