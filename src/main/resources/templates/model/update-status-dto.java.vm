package ${package.Entity};

import com.cauli.netdisk.base.model.dto.BaseDTO;
#foreach($pkg in ${table.importPackages})
import ${pkg};
#end
#if(${springdoc})
import io.swagger.v3.oas.annotations.media.Schema;
#elseif(${swagger})
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
#end
#if(${entityLombokModel})
import lombok.Data;
import lombok.EqualsAndHashCode;
#end

import javax.validation.constraints.NotNull;

/**
 * @author ${author}
 * @date ${date} ${time}
 * @description ${table.name}状态修改参数
 */
#if(${entityLombokModel})
@Data
@EqualsAndHashCode(callSuper = true)
#end
#if(${springdoc})
@Schema(name = "${entity}", description = "$!{table.comment}")
#elseif(${swagger})
@ApiModel(value = "${entity}UpdateStatusDTO", description = "${table.name}状态修改参数")
#end
public class ${entity}UpdateStatusDTO extends BaseDTO {
  @NotNull(message = "${table.name}ID不能为空")
  @ApiModelProperty("${table.name}ID")
  private Long ${table.name}Id;

  @NotNull(message = "${table.name}状态不能为空")
  @ApiModelProperty("${table.name}状态（0停用 1正常）")
  private Integer status;
}
