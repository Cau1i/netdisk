package ${package.Service};

import ${package.Entity}.${entity};
import ${superServiceClassPackage};

/**
 * @author ${author}
 * @date ${date} ${time}
 * @description $!{table.comment} 服务类
 */
public interface ${table.serviceName} extends ${superServiceClass}<${entity}> {
    /**
     * 分页获得${table.name}信息
     *
     * @param ${table.name}QueryDTO
     * @return
     */
    Page<${entity}> get${entity}Page(${entity}QueryDTO ${table.name}QueryDTO);

    /**
     * 根据${table.name}ID获取详细信息
     *
     * @param ${table.name}Id
     * @return
     */
    ${entity}DTO get${entity}ById(Long ${table.name}Id);

    /**
     * 添加${table.name}
     *
     * @param ${table.name}AddDTO
     */
    void add${entity}(${entity}AddDTO ${table.name}AddDTO);

    /**
     * 修改${table.name}
     *
     * @param ${table.name}UpdateDTO
     */
    void update${entity}(${entity}UpdateDTO ${table.name}UpdateDTO);

    /**
     * 修改${table.name}状态
     *
     * @param ${table.name}UpdateStatusDTO
     */
    void change${entity}Status(${entity}UpdateStatusDTO ${table.name}UpdateStatusDTO);

    /**
     * 删除${table.name}
     *
     * @param ${table.name}Ids
     */
    void delete${entity}(List<Long> ${table.name}Ids);
}